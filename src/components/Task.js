import { Button, Container, Grid, List, ListItem, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

import { inputChangeHandler, addTaskClickAction, toggleTaskClick } from "../actions/task.actions";

const Task = () => {
    const dispatch = useDispatch();

    // Lấy state từ redux
    const { inputString, taskList} = useSelector((reduxData) => {
        return reduxData.taskReducer;
    })

    // Khai báo các action
    const inputTaskChangeHandler = (event) => {
        dispatch(inputChangeHandler(event.target.value));
    }

    const addTaskClickHandler = () => {
        dispatch(addTaskClickAction());
    }

    const toggleClickHandler = (index) => {
        dispatch(toggleTaskClick(index));
    }

    return (
        <Container>
            <Grid container mt={5} alignItems="center">
                <Grid item xs={12} md={6} lg={8} sm={12}>
                <TextField label="Input task here" variant="outlined" fullWidth value={inputString} onChange={inputTaskChangeHandler}/>
                </Grid>
                <Grid item xs={12} md={6} lg={4} sm={12} textAlign="center">
                <Button variant="contained" onClick={addTaskClickHandler}>ADD TASK</Button>
                </Grid>
            </Grid>
            <Grid container>
                <List>
                    {taskList.map((element, index) => {
                        return <ListItem key={index} onClick={() => toggleClickHandler(index)} style={{color: element.status ? "green" : "red"}}>{index +1}. {element.taskName}</ListItem>
                    })}
                </List>
            </Grid>
        </Container>
    )
}

export default Task;
