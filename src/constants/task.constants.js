export const TASK_INPUT_CHANGE = "Sự kiện thay đổi input khi nhập task";
export const TASK_ADD_CLICKED = "Sự kiện khi ấn nút Add Task";
export const TASK_TOGGLE_CLICKED = "Sự kiện khi nhấn vào một Task thì thay đổi trạng thái của task đó"