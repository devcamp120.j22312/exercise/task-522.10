import { TASK_INPUT_CHANGE, TASK_ADD_CLICKED, TASK_TOGGLE_CLICKED } from "../constants/task.constants";

// Nơi mô tả những sự kiện liên quan đến task (Chứa các hàm return về mô tả)
const inputChangeHandler = (inputValue) => {
    return {
        type: TASK_INPUT_CHANGE,
        payload: inputValue
    }
}

const addTaskClickAction = () => {
    return {
        type: TASK_ADD_CLICKED,

    }
}

const toggleTaskClick = (index) => {
    return {
        type: TASK_TOGGLE_CLICKED,
        payload: index
    }
}

export {
    inputChangeHandler,
    addTaskClickAction,
    toggleTaskClick
}